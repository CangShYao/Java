import javax.swing.*;

/**
 * @program: DataManager
 * @description:
 * @author: jiandong
 * @create: 2018-05-17 19:46
 **/
public class DataManager {

    private JPanel panel1;
    private JTree tree1;
    private JButton fileButton;
    private JButton viewButton;
    private JButton commandButton;
    private JButton recentButton;
    private JButton optionsButton;
    private JButton clearButton;
    private JButton executeButton;
    private JTextArea commandArea;
    private JTextArea messageArea;

    private static void exec()
    {
        JFrame jFrame = new JFrame("Database Manager");
        // jFrame.setResizable(false);
        jFrame.setSize(700, 500);
        jFrame.setLocation(600, 300);
        jFrame.setContentPane(new DataManager().panel1);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // jFrame.pack();
        jFrame.setVisible(true);
    }

    public static void main(String[] args)
    {
        exec();
    }
}
