/**
 * @program: MineClearance
 * @description: Mine clearance
 * @author: jiandong
 * @create: 2018-04-21 15:04
 **/
import com.sun.javafx.collections.MappingChange;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Map;
import javax.swing.*;

public class Mine {
    private final int EMPTY = 0;
    private final int MINE = 1;
    private final int CHECKED = 2;
    private final int MINE_COUNT = 10;
    private final int MINE_SIZE = 10;
    private int FlagCount = MINE_COUNT;
    private boolean flag;
    private JFrame jFrame;
    private JButton[][] jButtons;
    private JButton newGame;
    private JPanel jPanelTop;
    private JPanel jPanelDown;
    private JLabel[] jLabels;
    private int[][] map;

    private int [][] move = {{-1, 0}, {-1, 1}, {0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}};

    public Mine()
    {
        jFrame = new JFrame("扫雷");                      // 添加标题
        jButtons = new JButton[MINE_SIZE][MINE_SIZE];           //设置一定大小的button
        jPanelTop = new JPanel();                               //设置雷区上面的Flag数目和time，还有重玩按钮
        jPanelDown = new JPanel();                              //雷区所在的地方
        jLabels = new JLabel[4];
        /* label 0， 显示Flag这个字符
        *  label 1， 显示flag现存的数量
        *  label 2， 显示Time这个字符
        *  label 3， 显示现在已经用掉的时间，单位是s（秒）
        */
        jLabels[0] = new JLabel("Flag：", JLabel.RIGHT);
        jLabels[1] = new JLabel("10", JLabel.LEFT);
        jLabels[2] = new JLabel("Time：", JLabel.RIGHT);
        jLabels[3] = new JLabel("0s", JLabel.LEFT);
        for (int i = 0; i < 4; i++)
            jLabels[i].setFont(new Font("", Font.BOLD, 34));//设置四个label的字体大小和字体类型
        newGame = new JButton("Replay");                 // 重新游戏按钮
        newGame.addActionListener(new ActionListener() {      // 重新开始游戏按钮响应事件
            @Override
            public void actionPerformed(ActionEvent e) {
                flag = false;                                 // 游戏结束？false
                FlagCount = MINE_SIZE;                        // 重置雷的数量
                setFlagNumber();                              // 在label上设置当前雷的数目
                for (int i = 0; i < MINE_SIZE; i++)
                {
                    for (int j = 0; j < MINE_SIZE; j++)
                    {
                        new mineTimer().start();
                        map[i][j] = EMPTY;                    // 把之前设置的雷清除掉，方便以后来放置
                        jButtons[i][j].setEnabled(true);      // 所有按钮要能使用
                        if (jButtons[i][j].getIcon() != null) // 有图标的要清除掉图标
                            jButtons[i][j].setIcon(null);
                        if (jButtons[i][j].getText() != null) // 有数字的要清除掉数字
                            jButtons[i][j].setText(null);
                    }
                }
                putMINE();                                    // 放置雷
                new Thread(new mineTimer()).start();
            }
        });
        //Top，顶部交互栏的布局
        jPanelTop.setLayout(new GridLayout(1, 5));
        jPanelTop.add(jLabels[0]);
        jPanelTop.add(jLabels[1]);
        jPanelTop.add(newGame);
        jPanelTop.add(jLabels[2]);
        jPanelTop.add(jLabels[3]);

        map = new int[MINE_SIZE][MINE_SIZE];                  // 用来存放周围有几颗雷的数组
        init();
        new mineTimer().start();
    }

    private void init()                                       // 初始化函数，目的就是不让构造函数看起来那么臃肿
    {
        jFrame.setSize(600, 640);                // 设置容器大小
        jFrame.setLocation(700, 100);                    // 设置容器初始出来的位置
        jFrame.setResizable(false);                           // 不能随意改变大小
        putBUTTON();                                          // 放置按钮
        jFrame.add(jPanelTop, BorderLayout.NORTH);            // 将top放在容器的最上面
        jFrame.add(jPanelDown);                               // down就放在top的下面，紧贴着的下面
        jFrame.setVisible(true);                              // 设置可见，不然什么都没有
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);  // 设置默认的×的作用
        flag = false;                                         // 游戏结束？ flag， 即false
        putMINE();                                            // 放置雷
    }

    private void putMINE()                                    // 放置雷
    {
        int i = 0, tempX, tempY;
        for (;i<MINE_COUNT;)
        {
            tempX = (int) (Math.random()*MINE_SIZE);
            tempY = (int) (Math.random()*MINE_SIZE);
            if (map[tempX][tempY] == EMPTY)
            {
                map[tempX][tempY] = MINE;
                i++;
            }
        }
    }

    private void putBUTTON()
    {
        jPanelDown.setLayout(new GridLayout(MINE_SIZE, MINE_SIZE));
        for (int i = 0; i < MINE_SIZE; i++)
        {
            for (int j = 0; j < MINE_SIZE; j++)
            {
                jButtons[i][j] = new JButton();
                jButtons[i][j].addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        if (flag) return;
                        Object object = e.getSource();
                        int x, y;
                        String[] tempStr = ((JButton) object).getName().split("-");
                        x = Integer.parseInt(tempStr[0]);
                        y = Integer.parseInt(tempStr[1]);
                        if (jButtons[x][y].getIcon() != null) return;
                        if (map[x][y] == MINE)
                        {
                            flag = true;
                            showMine();
                            return;
                        }
                        dfs(x, y, 0);
                        checkSuccess();
                    }
                });
                jButtons[i][j].addMouseListener(new MouseListener() {
                    @Override
                    public void mouseClicked(MouseEvent e) {
                        if (flag) return;                           // flag == true，那么说明你踩到了雷了，或者你扫完所有雷，那么不能继续点击雷区
                        if (e.getButton() == 3)                     // 鼠标右键点击
                        {
                            Object object = e.getSource();
                            String[] tempStr = ((JButton) object).getName().split("-");
                            int x = Integer.parseInt(tempStr[0]);
                            int y = Integer.parseInt(tempStr[1]);
                            if (!jButtons[x][y].isEnabled())        // 你点到了被排除的空白，那么也不能点击
                                return;
                            if (jButtons[x][y].getIcon() == null) { // 没有图片，那么就设置要给flag，你插了一个旗子
                                if (FlagCount != 0) {               // 插旗是有数目限制的
                                    jButtons[x][y].setIcon(new ImageIcon(this.getClass().getResource("flag.jpg")));
                                    FlagCount--;
                                    setFlagNumber();
                                }
                            } else {                                // 如果之前就有图片，那么现在就取消掉这个图片（要明白，在玩的时候只可能出现flag图片，要是出现雷的图片，那么一定游戏结束）
                                jButtons[x][y].setIcon(null);
                                FlagCount++;
                                setFlagNumber();
                            }

                        }
                    }
                    // 下面几个都没用，但是不敢删除。
                    @Override
                    public void mousePressed(MouseEvent e) {

                    }

                    @Override
                    public void mouseReleased(MouseEvent e) {

                    }

                    @Override
                    public void mouseEntered(MouseEvent e) {

                    }

                    @Override
                    public void mouseExited(MouseEvent e) {

                    }
                });
                jButtons[i][j].setName(i+"-"+j);                   // 设置按钮一个名字，目的是后面方便判断是哪个按钮被点击了
                jPanelDown.add(jButtons[i][j]);                    // 放置button
            }
        }
    }

    private void checkSuccess()                 // 用于判定游戏赢的部分，因为输的部分是实时检测的
    {
        int count = 0;
        for (int i = 0; i < MINE_SIZE; i++)
        {
            for (int j = 0; j < MINE_SIZE; j++)
            {
                if (jButtons[i][j].isEnabled())
                {
                    count++;
                }
            }
        }
        if (count == MINE_COUNT)                // 当剩下可用button数目和雷数一样时，赢的游戏
        {
            flag = false;
            showMine();
        }
    }

    private int dfs(int x, int y, int d)        //dfs算法来判断周围有几颗雷，以及显示所有相连的空白
    {
        map[x][y] = CHECKED;
        int tx, ty, count = 0;
        for (int i = 0;i < 8; i++)
        {
            tx = x + move[i][0];
            ty = y + move[i][1];
            if (tx >= 0 && tx < MINE_SIZE && ty >= 0 && ty < MINE_SIZE)
            {
                if (map[tx][ty] == MINE)
                {
                    count++;
                }
                else if (map[tx][ty] == EMPTY) ;
                else if (map[tx][ty] == CHECKED) ;
            }
        }
        if (count == 0)
        {
            for (int i = 0;i < 8; i++) {
                tx = x + move[i][0];
                ty = y + move[i][1];
                if (tx >= 0 && tx < MINE_SIZE && ty >= 0 && ty < MINE_SIZE && map[tx][ty] != CHECKED) {
                    dfs(tx, ty, d + 1);
                }
            }
        }
        else{
                jButtons[x][y].setText(count+"");
        }
        jButtons[x][y].setEnabled(false);                   //将空白button不能使用
        return count;
    }

    private void showMine()                                 //显示雷的位置
    {
        //先显示雷的位置
        Icon iconMine = new ImageIcon(this.getClass().getResource("mine.jpg"));
        for (int i = 0; i < MINE_SIZE; i++)
        {
            for (int j = 0; j < MINE_SIZE; j++)
            {
                if (map[i][j] == MINE)
                {
                    if (jButtons[i][j].getIcon() != null)   //当雷被标记时，这个雷显示为“死雷”，也就是在雷的图片上打上一个红色的×
                    {
                        Icon iconMineDie = new ImageIcon(this.getClass().getResource("mineDie.jpg"));
                        jButtons[i][j].setIcon(iconMineDie);
                    }
                    else jButtons[i][j].setIcon(iconMine);
                }
            }
        }
        //然后判断游戏输赢，要是赢了，弹出You are win的弹窗，否则，弹出Game over
        if (flag)
            JOptionPane.showMessageDialog(jFrame, "Game Over!!");
        else {
            flag = true;
            JOptionPane.showMessageDialog(jFrame, "You are win!");
        }
    }

    private void setFlagNumber()                            //在label中设置雷的数目
    {
        jLabels[1].setText(FlagCount+"");
    }

    protected class mineTimer extends Thread
    {
        private int mTime = -1;
        public void run()
        {
            while (!flag)
            {
                mTime++;
                jLabels[3].setText(mTime+"s");
                try{
                    Thread.sleep(1000);
                }catch (InterruptedException e)
                {
                      e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args)
    {
        Mine mine = new Mine();
    }
}
