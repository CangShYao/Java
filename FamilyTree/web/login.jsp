<%--
  Created by IntelliJ IDEA.
  User: m1329
  Date: 2018/5/20
  Time: 20:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>账号登录</title>
    <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>
    <link rel="stylesheet" href="css/style.css">


</head>

<body>
<div class="wrapper">
    <form class="form-signin" method="post" action="login">
        <h2 class="form-signin-heading">请登录</h2>
        <input type="text" class="form-control" name="username" placeholder="手机号" required="" autofocus=""/>
        <input type="password" class="form-control" name="password" placeholder="密码" required=""/>
        <label class="checkbox">
            <input type="checkbox" value="remember-me" id="rememberMe" name="rememberMe">记住密码
        </label>
        <button class="btn btn-lg btn-primary btn-block" type="submit">登录</button>
        <button class="btn btn-lg btn-primary btn-block" type="button" onclick="window.location='register.jsp'">注册</button>
    </form>
</div>
</body>

</html>