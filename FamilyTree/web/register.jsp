<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- 重置文件 -->
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/style_register.css">
    <title>注册</title>
</head>
<body>
<div class="reg_div">
    <p>注册</p>
    <ul class="reg_ul">
        <form role="form" method="post" action="register">
            <li>
                <span>用户名：</span>
                <input type="text" name="username" value="" placeholder="手机号" class="reg_mobile">
                <span class="tip mobile_hint"></span>
            </li>
            <li>
                <span>密码：</span>
                <input type="password" name="password" placeholder="6-16位密码" class="reg_password">
                <span class="tip password_hint"></span>
            </li>
            <li>
                <span>确认密码：</span>
                <input type="password" name="" placeholder="确认密码" class="reg_confirm">
                <span class="tip confirm_hint"></span>
            </li>
            <li>
                <span>父亲：</span>
                <input type="text" name="father" placeholder="父亲姓名">
            </li>
            <li>
                <span>母亲：</span>
                <input type="text" name="mother" placeholder="母亲姓名">
            </li>
            <li>
                <span>住址：</span>
                <input type="text" name="address" placeholder="家庭住址">
            </li>
            <li>
                <span>生日：</span>
                <input type="date" name="birthday">
                <span class="tip birthday_hint"></span>
            </li>
            <li>
                <button type="submit" name="button" class="red_button">注册</button>
            </li>
        </form>
    </ul>
</div>

<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
</body>
</html>