package family.domain;

/**
 * @program: FamilyTree
 * @description: 性别的判断
 * @author: jiandong
 * @create: 2018-05-22 19:29
 **/
public enum Sex {

    MALE, FEMALE, OTHER, NOT_KNOWN;
}
