package family.domain;

import java.util.HashSet;
import java.util.Set;

/**
 * @program: FamilyTree
 * @description: 家族成员信息化
 * @author: jiandong
 * @create: 2018-05-22 19:25
 **/
public class Person {
    private Long id;
    private String name;
    private family.domain.Person father;
    private family.domain.Person mother;
    private Sex sex;
    private Integer age;
    private Set<Person> children = new HashSet<Person>();

    public Person()
    {
        this.setSex(Sex.NOT_KNOWN);
    }

    private void setSex(Sex sex) {
        this.sex = sex;
    }

//    public static TypedQuery<Person> findChildren(Long parentId)
//    {
//
//    }
}
