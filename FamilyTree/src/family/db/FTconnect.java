package family.db;
import java.sql.*;
import java.sql.DriverManager;
import java.sql.Connection;

/**
 * @program: FamilyTree
 * @description:
 * @author: jiandong
 * @create: 2018-05-25 21:11
 **/
public class FTconnect {
    private static Connection connection = null;
    private static Statement stat = null;

    // 构造函数，用来初始化一个 数据库 连接
    // com.mysql.jdbc.Driver
    public FTconnect()
    {
        try{
            Class.forName("com.mysql.cj.jdbc.Driver");
            String dbUserName = "root";
            String dbPassword = "1234";
            String url = "jdbc:mysql://localhost:3306/winuser?serverTimezone=GMT%2B8&useSSL=true";
            connection = DriverManager.getConnection(url, dbUserName, dbPassword);
            stat = connection.createStatement();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    // 新建 数据库 查询，或者其他什么语句，也就是说，执行一些数据库操作
    public String getInformation(String sql)
    {
        try {
            ResultSet res = stat.executeQuery("SELECT password FROM family WHERE username='" + sql + "'");
            if (res.next()) return res.getString(1);
            else return "";
        }
        catch (SQLException se)
        {
            se.printStackTrace();
            return "";
        }
    }

    // 断开 数据库 连接
    public void disConnection()
    {
        try {
            stat.close();
            connection.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
}
