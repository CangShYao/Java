package family.sever;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import family.db.FTconnect;

@WebServlet(name = "register")
public class register extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String fatherName = request.getParameter("father");
        String motherName = request.getParameter("mother");
        String address = request.getParameter("address");
        try {
            FTconnect conn = new FTconnect();
            conn.disConnection();
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        response.sendRedirect("login.jsp");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }

    // 制造一些用来写文件的SQL语句
    private String makeSql(String sqlSource, String sqlValue)
    {
        return "insert into family " + sqlSource + "values" + sqlValue;
    }
}