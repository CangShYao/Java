package family.sever;

import family.db.FTconnect;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "login")
public class login extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        System.out.println("username:" + username + "\t" + "password:" + password);
        String psd;
        try {
            FTconnect conn = new FTconnect();
            psd = conn.getInformation(username);
            // 账号存在，密码正确，则跳转到showTree.jsp
            if (password.equals(psd)) {
                System.out.println("login successful");
                response.sendRedirect("showTree.jsp");
            }
            // 账号不存在，或者密码错误，就跳转到login.jsp
            else {
                response.sendRedirect("login.jsp");
            }
            conn.disConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }
}
