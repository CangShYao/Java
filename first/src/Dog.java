/**
 * @program: first
 * @description: Create a object
 * @author: jiandong
 * @create: 2018-03-19 11:55
 **/
public class Dog {
    public Dog(String name){
        System.out.println("The dog's name is " + name);
    }
    public static void main(String[] args)
    {
        Dog myDog = new Dog("yuanfei");
    }
}
