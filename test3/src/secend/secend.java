package secend;

/**
 * @program: test3
 * @description: secend for test
 * @author: jiandong
 * @create: 2018-05-16 22:18
 **/
import java.text.NumberFormat;
public class secend {
    public void fun()
    {
        System.out.println("Hello java package!!");
        System.out.println("Default location:");
        NumberFormat moneyFormater =
                NumberFormat.getCurrencyInstance();
        System.out.println(moneyFormater.format(19.8));
        System.out.println(moneyFormater.format(19.81111));
        System.out.println(moneyFormater.format(19.89999));
        System.out.println(moneyFormater.format(19));
        System.out.println();
    }
}
