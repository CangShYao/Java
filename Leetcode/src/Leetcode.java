/**
 * @program: Leetcode
 * @description: leetcode 416 Partition Equal Subset Sum
 * @author: jiandong
 * @create: 2018-03-26 16:08
 **/
public class Leetcode {
    private static boolean canPartition(int[] nums) {
        int sum = 0;
        for (int x : nums) {
            sum += x;
        }
        if (sum % 2 != 0) return false; // if the sum is odd number, the array can't be partition to equal subst sum
        sum /= 2;
        boolean[][] dp = new boolean[nums.length + 1][sum + 1]; // create a 2D array of boolean
        // initialize the array of dp whit false
        for (int i = 0; i < nums.length + 1; ++i) {
            for (int j = 0; j < sum + 1; ++j) {
                if (i == 0 || j == 0) {
                    dp[i][j] = false;
                }
            }
        }
        dp[0][0] = true; // the first of dp is true
        // convert to 0-1 backpack problem
        for (int i = 1; i < nums.length + 1; ++i) {
            for (int j = 1; j < sum + 1; ++j) {
                // j is current contain of backpack
                if (j >= nums[i - 1]) {
                    // if current contain is big the value of goods, put it or not put it into backpack
                    dp[i][j] = dp[i - 1][j] || dp[i - 1][j - nums[i - 1]];
                } else dp[i][j] = dp[i - 1][j]; // if contain is not enough, don't put it into backpack
            }
        }
        return dp[nums.length][sum]; // return the largest
    }

    public static void main(String[] args) {
        int[] nums = {1, 5, 5, 11};
        if (canPartition(nums)) {
            System.out.print("true");
        } else {
            System.out.print("false");
        }
    }
}