/**
 * @program: test1
 * @description: test
 * @author: jiandong
 * @create: 2018-04-23 14:00
 **/
import com.sun.xml.internal.messaging.saaj.soap.JpegDataContentHandler;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Timer;


public class win {
    private JFrame jFrame;
    private JButton[][] jButtons;
    private JPanel jPanelTop;
    private JPanel jPanelDown;
    private JLabel jLabel;
//    private JMenuBar jMenuBar;
//    private JMenu jMenu;


    public win(int row, int cols)
    {
        jPanelTop = new JPanel();
        jPanelDown = new JPanel();
        jLabel = new JLabel("", JLabel.CENTER);
        jLabel.setFont(new Font("",Font.BOLD, 34));
        jPanelTop.setLayout(new GridLayout(1, 3));
        jPanelTop.add(jLabel);
        jPanelTop.add(new JButton("Button"));
        jPanelTop.add(new JLabel("Label 2", JLabel.CENTER));
        timer();


        jFrame = new JFrame("test");
        jFrame.setLocation(700, 200);
        jFrame.setSize(500, 550);
        jPanelDown.setLayout(new GridLayout(row, cols));
        jButtons = new JButton[row][cols];
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < cols; j++)
            {
                jButtons[i][j] = new JButton(i*cols+j+1+"");
                //jButtons[i][j].setSize(50,50);
                jPanelDown.add(jButtons[i][j]);
            }
        }
        jFrame.add(jPanelTop, BorderLayout.NORTH);
        jFrame.add(jPanelDown);
        jFrame.setVisible(true);
        jFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    protected void timer()
    {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new JLabelTimeShow(),new Date(), 1000);
    }

    protected class JLabelTimeShow extends TimerTask
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        public void run()
        {
            String time = dateFormat.format(Calendar.getInstance().getTime());
            jLabel.setText(time);
        }
    }

    public static void main(String[] args)
    {
        win w = new win(8, 8);
    }
}
