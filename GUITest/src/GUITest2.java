/**
 * @program: GUITest
 * @description: GUI test java file, the second version
 * @author: jiandong
 * @create: 2018-03-30 22:03
 **/
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

public class GUITest2 {
    private Frame frame;
    private TextField textField;
    private Button button;
    private TextArea textArea;
    private Dialog dialog;
    private Label label;
    private Button okButton;

    GUITest2()
    {
        init();
    }

    private void init()
    {
        frame = new Frame("Test 2");
        frame.setBounds(300, 100, 600, 500);
        frame.setLayout(new FlowLayout());

        textField = new TextField(60);
        button = new Button("Go to");
        textArea = new TextArea(25, 70);

        dialog = new Dialog(frame, "Information", true);
        dialog.setBounds(400, 200, 250, 150);
        dialog.setLayout(new FlowLayout());

        label = new Label();
        okButton = new Button("OK");

        dialog.add(label);
        dialog.add(okButton);

        frame.add(textField);
        frame.add(button);
        frame.add(textArea);

        myEvent();

        frame.setVisible(true);
    }

    private void myEvent()
    {
        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                System.exit(0);
            }
        });
    }

    public static void main(String[] args)
    {
        new GUITest2();
    }
}
