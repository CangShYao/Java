/**
 * @program: GUITest
 * @description: GUI test java file
 * @author: jiandong
 * @create: 2018-03-29 17:47
 **/
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class GUITest1 {
    public static void main(String[] args)
    {
        Frame f = new Frame("Dlog");
        f.setSize(500, 400);
        f.setLocation(300, 200);
        f.setLayout(new FlowLayout());

        Button b = new Button("I am a button");
        b.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("exit, button do it!");
                System.exit(0);
            }
        });

        TextField tf = new TextField(20);
        f.add(b);
        f.add(tf);
        f.addWindowListener(new MyWindow());
        f.setVisible(true);
        System.out.println("hello world!");
    }
}

class MyWindow extends WindowAdapter{
    public void windowClosing(WindowEvent event)
    {
        System.out.println("close it!");
        System.exit(0);
    }

    public void windowOpened(WindowEvent event)
    {
        System.out.println("open it!");
    }

    public void windowActivated(WindowEvent event)
    {
        System.out.println("active now!");
    }
}
