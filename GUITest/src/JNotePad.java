/**
 * @program: GUITest
 * @description: a note software
 * @author: jiandong
 * @create: 2018-03-30 22:33
 **/

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;

public class JNotePad {
    private Frame noteFrame;
    private MenuBar noteMenuBar;
    private Menu noteMenu, subm;
    private MenuItem closeItem, openItem, saveItem, subItem, subItem2;
    private FileDialog openDialog, saveDialog;
    private TextArea noteTextArea;
    private File noteFile;

    private JNotePad() {
        init();
    }

    private void init() {
        noteFrame = new Frame("Note Pad ++");
        noteFrame.setBounds(300, 100, 600, 500);

        noteMenuBar = new MenuBar();

        noteMenu = new Menu("File");
        closeItem = new MenuItem("Close");
        openItem = new MenuItem("Open");
        saveItem = new MenuItem("Save");

        subm = new Menu("New");
        subItem = new MenuItem("TXT file");
        subItem2 = new MenuItem("Java file");
        subm.add(subItem);
        subm.add(subItem2);

        noteMenu.add(subm);
        noteMenu.add(openItem);
        noteMenu.add(saveItem);
        noteMenu.add(closeItem);

        noteMenuBar.add(noteMenu);

        openDialog = new FileDialog(noteFrame, "open file", FileDialog.LOAD);
        saveDialog = new FileDialog(noteFrame, "save file", FileDialog.SAVE);

        noteTextArea = new TextArea();
        Font font = new Font("Courier", Font.PLAIN, 19);
        noteTextArea.setFont(font);

        noteFrame.add(noteTextArea);
        noteFrame.setMenuBar(noteMenuBar);

        MyEvent();

        noteFrame.setVisible(true);
    }

    private void MyEvent() {
        saveItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (noteFile == null) {
                    saveDialog.setVisible(true);
                    String dirPath = saveDialog.getDirectory();
                    String fileName = saveDialog.getFile();
                    if (dirPath == null || fileName == null)
                        return;
                    noteFile = new File(dirPath, fileName);
//                    noteFile = new File(dirPath, fileName+".txt");
                }

                try {
                    BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(noteFile));
                    String text = noteTextArea.getText();
                    bufferedWriter.write(text);
                    bufferedWriter.close();
                } catch (IOException e2) {
                    throw new RuntimeException("save failed!");
                }
            }
        });

        openItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                openDialog.setVisible(true);
                String dirPath = openDialog.getDirectory();
                String fileName = openDialog.getFile();
                if (fileName == null || dirPath == null)
                    return;
                noteTextArea.setText("");
                noteFile = new File(dirPath, fileName);
                try {
                    BufferedReader bufr = new BufferedReader(new FileReader(noteFile));
                    String line = null;
                    while ((line = bufr.readLine()) != null)
                        noteTextArea.setText(line);
                    bufr.close();
                } catch (IOException e2) {
                    throw new RuntimeException("open exception");
                }
            }
        });

        closeItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        noteFrame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                System.exit(0);
            }
        });
    }

    public static void main(String[] args) {
        new JNotePad();
    }
}
