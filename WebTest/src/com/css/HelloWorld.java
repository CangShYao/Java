package com.css;

import javax.servlet.*;
import java.io.IOException;

/**
 * @program: WebTest
 * @description: helloworld
 * @author: jiandong
 * @create: 2018-05-22 14:00
 **/
public class HelloWorld implements Servlet{

    @Override
    public void init(ServletConfig servletConfig) throws ServletException {
        System.out.println("--init--");
    }

    @Override
    public ServletConfig getServletConfig() {
        return null;
    }

    @Override
    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
        System.out.println("--service--");
    }

    @Override
    public String getServletInfo() {
        return null;
    }

    @Override
    public void destroy() {
        System.out.println("--destroy--");
    }
}
