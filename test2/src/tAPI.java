public interface tAPI <T extends Comparable<T>> {
    public boolean insert(T x);
    public void visit();
    public boolean search(T x);
}
