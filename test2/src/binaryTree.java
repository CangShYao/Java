/**
 * @program: test2
 * @description: Binary tree
 * @author: jiandong
 * @create: 2018-05-03 18:58
 **/
public class binaryTree<T extends Comparable<T>> implements tAPI<T>{
    public class binaryNode<T>{
        T data;
        binaryNode<T> leftNode;
        binaryNode<T> rightNode;
        private binaryNode(T x)
        {
            data = x;
            leftNode = null;
            rightNode = null;
        }
    }

    private binaryNode<T> root;

    public boolean insert(T x)
    {
        if (root == null)
            root = new binaryNode<>(x);
        binaryNode<T> current = root;
        binaryNode<T> parent = null;
        while (current!=null)
        {
            int temp = current.data.compareTo(x);
            if (temp > 0)
            {
                parent = current;
                current = current.leftNode;
            }
            else if (temp < 0)
            {
                parent = current;
                current = current.rightNode;
            }
            else
            {
                return false;
            }
        }
        int temp = parent.data.compareTo(x);
        if (temp > 0)
            parent.leftNode = new binaryNode<>(x);
        else
            parent.rightNode = new binaryNode<>(x);

        return true;
    }
    public void visit()
    {
        
    }

    public boolean search(T x)
    {
        binaryNode<T> node = root;
        while (node != null)
        {
            int temp = node.data.compareTo(x);
            if (temp > 0)
                node = node.leftNode;
            else if (temp < 0)
                node = node.rightNode;
            else
                return true;
        }
        return false;
    }

    private binaryNode node; // 私有成员变量，二叉节点
    // 二叉搜索树的构建
    public binaryTree(T[] arr)
    {
        for (int i = 0; i < arr.length; i++)
        {
            insert(arr[i]);
        }
    }

    // 二叉搜索树的销毁


    public static void main(String[] args)
    {
        Integer arr[] = {5, 2, 3, 1, 8, 9, 7, 6, 4};
        binaryTree<Integer> s = new binaryTree<>(arr);
        if (s.insert(10))
            System.out.println("insert success");
    }
}
