package Model;

import java.util.Observable;

class Model extends Observable {
    final int BALL_SIZE = 20;
    private int xPosition = 0;
    private int yPosition = 0;
    private int xLimit, yLimit;
    private int xDelta = 6;
    private int yDelta = 4;

    void makeOneStep() {
        xPosition += xDelta;
        if (xPosition < 0) {
            xPosition = 0;
            xDelta = -xDelta;
        }
        if (xPosition >= xLimit) {
            xPosition = xLimit;
            xDelta = -xDelta;
        }
        yPosition += yDelta;
        if (yPosition < 0 || yPosition >= yLimit) {
            yDelta = -yDelta;
            yPosition = yDelta;
        }
        setChanged();
        notifyObservers();
    }
}
