package MNS;

import com.aliyun.mns.client.CloudAccount;
import com.aliyun.mns.client.CloudQueue;
import com.aliyun.mns.client.MNSClient;
import com.aliyun.mns.common.ClientException;
import com.aliyun.mns.common.ServiceException;
import com.aliyun.mns.common.utils.ServiceSettings;
import com.aliyun.mns.model.Message;
import com.aliyun.mns.model.QueueMeta;

public class MNSQueue {
    private CloudAccount account;
    private MNSClient client;

    public MNSQueue() {
        account = new CloudAccount(
                ServiceSettings.getMNSAccessKeyId(),
                ServiceSettings.getMNSAccessKeySecret(),
                ServiceSettings.getMNSAccountEndpoint()
        );
        client = account.getMNSClient();
    }

    public String createQueue(int time, String name) // the Queue name
    {
        if (!client.isOpen())
            client = account.getMNSClient();
        try {
            CloudQueue queue = client.getQueueRef(name);
            if (queue.isQueueExist()) {
                return "Create failed! Because the queue " + name + " is exist!\n";
            } else {
                QueueMeta queueMeta = new QueueMeta();
                queueMeta.setQueueName(name);
                queueMeta.setPollingWaitSeconds(time);
                CloudQueue cloudQueue = client.createQueue(queueMeta);
                return "Create " + name + " successfully. URL: " + cloudQueue.getQueueURL() + "\n";
            }

        } catch (ClientException ce) {
            ce.printStackTrace();
            return "Something wrong with the network connection between client and MNS service."
                    + "Please check your network and DNS availability.\n";
        } catch (ServiceException se) {
            se.printStackTrace();
            if (se.getErrorCode().equals("QueueNotExist")) {
                return "Queue is not exist.Please createQueue before use\n";
            } else if (se.getErrorCode().equals("TimeExpired")) {
                return "The request is time expired. Please check your local machine timeclock\n";
            }
            /*
            you can get more MNS service error code in following link.
            https://help.aliyun.com/document_detail/mns/api_reference/error_code/error_code.html?spm=5176.docmns/api_reference/error_code/error_response
            */
        } catch (Exception e) {
            e.printStackTrace();
            return "Unknown exception happened!\n";
        }
        client.close();
        return "";
    }

    public String deleteQueue(String name) // the Queue name
    {
        if (!client.isOpen())
            client = account.getMNSClient();
        try {   //Delete Queue
            CloudQueue queue = client.getQueueRef(name);
            if (queue.isQueueExist()) {
                queue.delete();
                return "Delete " + name + " successfully!\n";
            } else {
                return "Delete " + name + " failed! Because the queue is not exist!\n";
            }
        } catch (ClientException ce) {
            ce.printStackTrace();
            return "Something wrong with the network connection between client and MNS service."
                    + "Please check your network and DNS availability.\n";
        } catch (ServiceException se) {
            se.printStackTrace();
            if (se.getErrorCode().equals("QueueNotExist")) {
                return "Queue is not exist.Please create before use\n";
            } else if (se.getErrorCode().equals("TimeExpired")) {
                return "The request is time expired. Please check your local machine timeclock\n";
            }
            /*
            you can get more MNS service error code in following link.
            https://help.aliyun.com/document_detail/mns/api_reference/error_code/error_code.html?spm=5176.docmns/api_reference/error_code/error_response
            */
        } catch (Exception e) {
            e.printStackTrace();
            return "Unknown exception happened!\n";
        }

        client.close();
        return "";
    }

    public String getOneMessage(String name) // the Queue name
    {
        if (!client.isOpen())
            client = account.getMNSClient();
        try {
            CloudQueue queue = client.getQueueRef(name);// replace with your queue name
            Message popMsg = queue.popMessage();
            if (popMsg != null) {
                String handle = popMsg.getReceiptHandle();
                String body = popMsg.getMessageBodyAsString();
                String messageID = popMsg.getMessageId();
                int deleteQueueCount = popMsg.getDequeueCount();
                queue.deleteMessage(popMsg.getReceiptHandle());
                return "message handle: " + handle + "\n"
                        + "message body: " + body + "\n"
                        + "message id: " + messageID + "\n"
                        + "message dequeue count:" + deleteQueueCount + "\n";
            }
        } catch (ClientException ce) {
            ce.printStackTrace();
            return "Something wrong with the network connection between client and MNS service."
                    + "Please check your network and DNS availablity.\n";
        } catch (ServiceException se) {
            se.printStackTrace();
            if (se.getErrorCode().equals("QueueNotExist")) {
                return "Queue is not exist.Please create queue before use\n";
            } else if (se.getErrorCode().equals("TimeExpired")) {
                return "The request is time expired. Please check your local machine timeclock\n";
            }
            /*
            you can get more MNS service error code in following link.
            https://help.aliyun.com/document_detail/mns/api_reference/error_code/error_code.html?spm=5176.docmns/api_reference/error_code/error_response
            */
        } catch (Exception e) {
            e.printStackTrace();
            return "Unknown exception happened!\n";
        }
        client.close();
        return "";
    }

    public String sendOneMessage(String name, String body)//return a message id
    {
        if (!client.isOpen())
            client = account.getMNSClient();
        try{
            CloudQueue queue = client.getQueueRef(name);// replace with your queue name
            Message message = new Message();
            message.setMessageBody(body); // use your own message body here
            Message putMsg = queue.putMessage(message);
            return "Send message id is: " + putMsg.getMessageId()+"\n";
        } catch (ClientException ce)
        {
            ce.printStackTrace();
            return "Something wrong with the network connection between client and MNS service."
                    + "Please check your network and DNS availablity.\n";

        } catch (ServiceException se)
        {
            se.printStackTrace();
            if (se.getErrorCode().equals("QueueNotExist"))
            {
                return "Queue is not exist.Please create before use\n";
            } else if (se.getErrorCode().equals("TimeExpired"))
            {
                return "The request is time expired. Please check your local machine timeclock\n";
            }
            /*
            you can get more MNS service error code from following link:
            https://help.aliyun.com/document_detail/mns/api_reference/error_code/error_code.html?spm=5176.docmns/api_reference/error_code/error_response
            */
        } catch (Exception e)
        {
            e.printStackTrace();
            return "Unknown exception happened!\n";
        }

        client.close();

        return "";
    }
}
