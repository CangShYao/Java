package GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import MNS.*;

public class Queue_GUI {
    private String logs;
    public Queue_GUI() {
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MNSQueue mnsQueue = new MNSQueue();
                String qName = textField1.getText();
                if (qName.equals("")) {
                    logs = mnsQueue.createQueue(30, "cloud-queue-demo");
                    textArea2.append(logs+"\n");
                }
                else {
                    logs = mnsQueue.createQueue(30, qName);
                    textArea2.append(logs+"\n");
                }

            }
        });
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MNSQueue mnsQueue = new MNSQueue();
                String qName = textField1.getText();
                if (qName.equals(""))
                {
                    logs = mnsQueue.deleteQueue("cloud-queue-demo");
                    textArea2.append(logs+"\n");
                }
                else {
                    logs = mnsQueue.deleteQueue(qName);
                    textArea2.append(logs+"\n");
                }
            }
        });
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea2.setText("");
            }
        });
        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MNSQueue mnsQueue = new MNSQueue();
                String qName = textField1.getText();
                String qBody = textArea1.getText();
                if (qName.equals(""))
                {
                    logs = mnsQueue.sendOneMessage("cloud-queue-demo", qBody);
                    textArea2.append(logs+"\n");
                }
                else {
                    logs = mnsQueue.sendOneMessage(qName, qBody);
                    textArea2.append(logs+"\n");
                }
            }
        });
        receiveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MNSQueue mnsQueue = new MNSQueue();
                String qName = textField1.getText();
                if (qName.equals(""))
                {
                    logs = mnsQueue.getOneMessage("cloud-queue-demo");
                    textArea2.append(logs+"\n");
                }
                else {
                    logs = mnsQueue.getOneMessage(qName);
                    textArea2.append(logs+"\n");
                }
            }
        });
    }

    public void init()
    {
        JFrame frame = new JFrame("MNS");
        frame.setContentPane(new Queue_GUI().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setBounds(((Toolkit.getDefaultToolkit().getScreenSize().width)/2)-475,
                ((Toolkit.getDefaultToolkit().getScreenSize().height)/2)-300,
                950,600);
        frame.setMinimumSize(new Dimension(950, 500));
        frame.setVisible(true);
    }

    private JTextField textField1;
    private JPanel panel1;
    private JButton button1;
    private JButton button2;
    private JButton sendButton;
    private JButton receiveButton;
    private JButton removeButton;
    private JTextArea textArea2;
    private JButton button3;
    private JTextArea textArea1;
}
